<?php

namespace DominikWeber\HtmlMinifier\Hooks;

use DominikWeber\HtmlMinifier\Utility\Minifier;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;

class ContentPostProcessor
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
    }

    /**
     * @throws Exception
     */
    protected function minifyOutput()
    {
        $minifier = $this->objectManager->get(Minifier::class);
        $GLOBALS['TSFE']->content = $minifier->process($GLOBALS['TSFE']->content);
    }

    /**
     * render_Cache method
     * render method for cached pages.
     *
     * @throws Exception
     */
    public function render_Cache()
    {
        if (!$GLOBALS['TSFE']->isINTincScript()) {
            $this->minifyOutput();
        }
    }

    /**
     * @throws Exception
     */
    public function render_noCache()
    {
        if ($GLOBALS['TSFE']->isINTincScript()) {
            $this->minifyOutput();
        }
    }
}
